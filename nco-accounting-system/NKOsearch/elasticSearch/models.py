from django.db import models
from django_elasticsearch_dsl_drf.wrappers import dict_to_obj

class coord_livingnco(models.Model):
    city = models.TextField(primary_key=True)
    count_nco = models.IntegerField()
    population_size = models.IntegerField()
    longitude = models.FloatField()
    latitude = models.FloatField()

    class Meta:
        managed = False
        db_table = 'coord_livingnco'

    def __str__(self):
            return self.city


class dataModel(models.Model):
    data = models.TextField(blank=True, null=True)
    living_status = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.data