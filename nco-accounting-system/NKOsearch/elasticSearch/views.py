from django.shortcuts import render
from rest_framework import generics
from .models import coord_livingnco
from .serializers import TalkSerializer, NcoSerializer
from .documents import TalkDocument, LiveNcoDocument
from rest_framework.response import Response
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    CompoundSearchFilterBackend,
)
from django_elasticsearch_dsl_drf.viewsets import DocumentViewSet
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class MapList(generics.ListAPIView):
    queryset = TalkDocument.search()
    queryset = queryset[:queryset.count()]
    serializer_class = TalkSerializer

    @method_decorator(cache_page(60 * 60 * 2))
    def dispatch(self, *args, **kwargs):
        return super(MapList, self).dispatch(*args, **kwargs)


class NcoList(DocumentViewSet):
    serializer_class = NcoSerializer
    document = LiveNcoDocument
    filter_backends = [
        FilteringFilterBackend,
        CompoundSearchFilterBackend,
    ]
    search_fields = (
        'data.name.full',
        'data.ogrn',
    )
    filter_fields = {
        'area': 'area',
        'living_status': 'living_status'
    }

    def get_queryset(self):
        queryset = LiveNcoDocument.search()
        # queryset = queryset[:queryset.count()]
        queryset = queryset[:1000]
        return queryset

    @method_decorator(cache_page(60 * 60 * 2))
    def dispatch(self, *args, **kwargs):
        return super(NcoList, self).dispatch(*args, **kwargs)
