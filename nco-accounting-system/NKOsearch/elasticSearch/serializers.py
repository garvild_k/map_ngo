from rest_framework import serializers
from .models import coord_livingnco
from .documents import LiveNcoDocument
from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

class TalkSerializer(serializers.ModelSerializer):
    class Meta:
        model = coord_livingnco
        fields = (
            'city',
            'count_nco',
            'population_size',
            'longitude',
            'latitude',
        )

class NcoSerializer(DocumentSerializer):
    class Meta(object):
        document = LiveNcoDocument
        fields = (
            'data',
            'living_status',
        )