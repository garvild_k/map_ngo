from django_elasticsearch_dsl import Document, Index, fields
from .models import coord_livingnco, dataModel
from django_elasticsearch_dsl_drf.compat import KeywordField, StringField
from elasticsearch_dsl import analyzer


talks = Index('coord_livingnco')
livenco = Index('dadata_with_living_status')

html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)

@talks.document
class TalkDocument(Document):
    class Django:
        model = coord_livingnco
        fields = (
            'city',
            'count_nco',
            'population_size',
            'longitude',
            'latitude',
        )

@livenco.doc_type
class LiveNcoDocument(Document):
    data = fields.ObjectField()
    living_status = fields.TextField()

    class Django:
        model = dataModel