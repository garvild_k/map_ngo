from django.urls import path, re_path
from . import views
from elasticSearch.views import MapList, NcoList

urlpatterns = [
    path('api/v1/map/', MapList.as_view()),
    path('api/v1/nco/', NcoList.as_view({'get': 'list'})),
    re_path(r'.*', views.index),
]