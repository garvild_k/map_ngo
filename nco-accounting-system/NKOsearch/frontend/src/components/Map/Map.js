import React, {Component} from 'react';
import Spinner from '../General/Spinner/Spinner';
import {YMaps, Map, Placemark, Polygon} from 'react-yandex-maps';
import Sidebar from "./Sidebar";

class MainMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loaded: false,
            placeholder: "Loading"
        };
    }

    sortByCountNco(data) {
        data.sort((a, b) => a.count_nco > b.count_nco ? 1 : -1);
    }

    componentDidMount() {
        fetch("/api/v1/map/")
            .then(response => {
                if (response.status > 400) {
                    return this.setState(() => {
                        return {placeholder: "Something went wrong!",};
                    });
                }
                return response.json();
            })
            .then(data => {
                this.setState(() => {
                    this.sortByCountNco(data)
                    return {
                        data,
                        loaded: true,
                    };
                });
            });
    }

    colorsLevel(count_nco, population_size) {
        const palitra = ['#FF2020', '#AA2020', '#CC9900', '#20AA20', '#40FF40']
        const people = Math.round((count_nco / population_size) * 100000)
        var colors = Math.round(people / 20)
        if (colors >= 4) {
            colors = 4
        }
        return (palitra[colors])
    }

    innerSize(count_nco) {
        if (count_nco >= 300) {
            return (16)
        } else {
            return (10)
        }
    }

    coordMapCreatePlacemark() {
        return (
            <div>
                {this.state.data.map(coord => {
                    return (
                        <Placemark key={coord.city} geometry={[coord.longitude, coord.latitude]}
                                   modules={['layout.PieChart', 'geoObject.addon.hint']}
                                   properties={{
                                       data: [
                                           {
                                               weight: coord.count_nco,
                                               color: String(this.colorsLevel(+coord.count_nco, +coord.population_size)),
                                           },
                                       ],
                                       iconCaption: coord.city,
                                       hintContent: "Индекс территории: " + Math.round((coord.count_nco / coord.population_size) * 100000)
                                           + " СО НКО на 100 тыс. чел., всего " + Math.round(coord.population_size / 1000) + " тыс. чел.",
                                   }}
                                   options={{
                                       iconLayout: 'default#pieChart',
                                       iconPieChartRadius: 15 + (20 * Math.round((coord.count_nco / 1000))),
                                       iconPieChartCoreRadius: this.innerSize(coord.count_nco),
                                       iconPieChartCoreFillStyle: '#ffffff',
                                       iconPieChartStrokeStyle: '#ffffff',
                                       iconPieChartStrokeWidth: 3,
                                       iconPieChartCaptionMaxWidth: 200
                                   }}
                        />
                    );
                })}
            </div>
        )
    }

    coordMapAddPlacemark() {
        const myCoord = [
            [48.44419, 46.490641],
            [48.266626, 47.105513],
            [47.817216, 47.138472],
            [47.713516, 47.391158],
            [47.757985, 48.083297],
            [46.755746, 49.028121],
            [46.657498, 48.500777],
            [45.972378, 49.819137],
            [44.63116, 47.259322],
            [45.014285, 45.732222],
            [45.918729, 43.919478],
            [46.216965, 42.89775],
            [45.987697, 42.326461],
            [46.140649, 42.337447],
            [46.110093, 42.139693],
            [45.964717, 42.128707],
            [45.895721, 41.16191],
            [45.688213, 41.16191],
            [45.657405, 40.854293],
            [45.247586, 41.063033],
            [45.20877, 41.337691],
            [45.014285, 41.425582],
            [44.967509, 41.645308],
            [44.615467, 41.4915],
            [44.292836, 41.722213],
            [43.960474, 41.425582],
            [44.142715, 40.898238],
            [43.578328, 40.711471],
            [43.586314, 40.118209],
            [43.257994, 39.920455],
            [44.623314, 37.305709],
            [44.983105, 36.624556],
            [44.944107, 35.613814],
            [44.623314, 35.163375],
            [44.591921, 34.570113],
            [44.292836, 34.207564],
            [44.229674, 33.702193],
            [44.513364, 33.174849],
            [44.975308, 33.361617],
            [45.201003, 32.823287],
            [45.193235, 32.416793],
            [45.549442, 32.328902],
            [45.980038, 33.26274],
            [45.957055, 33.526412],
            [46.201711, 33.647262],
            [46.064226, 34.350387],
            [45.941728, 34.471236],
            [45.788221, 34.965621],
            [45.479927, 35.547896],
            [45.487655, 37.371627],
            [46.224591, 37.975875],
            [46.740643, 37.426558],
            [46.959226, 38.272506],
            [47.605993, 38.368049],
            [47.850603, 38.851447],
            [47.835811, 39.730354],
            [48.365651, 39.917121],
            [48.650356, 39.675422],
            [48.868265, 40.048957],
            [49.020236, 39.686408],
            [49.279675, 40.213752],
            [49.516295, 40.037971],
            [49.623471, 40.136848],
            [49.948374, 41.089102],
            [50.624108, 41.550527],
            [50.777606, 41.176992],
            [51.179866, 41.924062],
            [51.276439, 42.759023],
            [51.013839, 43.352285],
            [51.166053, 44.560781],
            [51.027697, 45.241934],
            [50.596145, 45.176016],
            [50.610128, 45.593496],
            [50.791536, 45.79125],
            [50.652054, 46.076894],
            [50.526164, 46.098867],
            [50.666021, 46.648184],
            [50.3828, 47.418985],
            [50.189226, 47.281656],
            [50.097442, 47.342081],
            [49.941712, 47.182779],
            [49.860106, 46.913614],
            [49.331662, 46.781778],
            [49.169844, 47.056437],
            [49.025558, 46.95756],
            [48.960492, 46.776285],
            [48.44419, 46.490641]
        ]

        return (
            <Map defaultState={{center: [47.222078, 39.720349], zoom: 9}}
                 width={'100%'} height={'100%'}>

                <Polygon
                    geometry={[
                        myCoord
                    ]}
                    options={{
                        fillColor: 'rgba(255,255,255,0.0)',
                        strokeColor: '#2C3E50',
                        opacity: 0.5,
                        strokeWidth: 5,
                        strokeStyle: 'shortdash',
                    }}
                />

                {this.coordMapCreatePlacemark()}

            </Map>
        )
    }

    render() {
        return (
            <React.Fragment>
                <div className={"grid-main max"}>
                    <YMaps>
                        {this.state.loaded ? this.coordMapAddPlacemark() : <Spinner/>}
                    </YMaps>
                </div>
                <div className={"footer wrapper-side"}>
                    <Sidebar/>
                </div>
            </React.Fragment>
        )
    }
}

export default MainMap;
