import React from 'react';

const Sidebar = () => {
    return (
        <React.Fragment>
            <input type="checkbox" id="check"/>
            <label htmlFor="check" style={{display: "block", background: "#2C3E50"}}>
                <i className="fa fa-angle-double-left fa-2x pr-1 pl-1" id="cancel"/>
                <i className="fa fa-times fa-2x pr-1 pl-1" id="arrow"/>
            </label>

            <div className="sidebar">
                <div className={"header-sidebar"}>
                    <div className={"container"}>
                        <div className={"header-text pt-5 pb-4"}>
                            Легенда
                        </div>
                    </div>
                </div>

                <div className={"container-legend"}>

                    <div className={"pb-2"}>

                        <div className={"leg-col c pb-1"}>
                            <div className={"col-sm"}>
                                <h5>Индекс территорий</h5>
                            </div>
                        </div>
                        <div className={"leg-col c"}>
                            <div className={"col-sm-3"}>
                                <hr className={"hr1"}/>
                            </div>
                            <div className={"col-sm"}>70 ></div>
                        </div>

                        <div className={"leg-col c"}>
                            <div className={"col-sm-3"}>
                                <hr className={"hr2"}/>
                            </div>
                            <div className={"col-sm"}>50-70</div>
                        </div>

                        <div className={"leg-col c"}>
                            <div className={"col-sm-3"}>
                                <hr className={"hr3"}/>
                            </div>
                            <div className={"col-sm"}>30-50</div>
                        </div>

                        <div className={"leg-col c"}>
                            <div className={"col-sm-3"}>
                                <hr className={"hr4"}/>
                            </div>
                            <div className={"col-sm"}>10-30</div>
                        </div>

                        <div className={"leg-col c"}>
                            <div className={"col-sm-3"}>
                                <hr className={"hr5"}/>
                            </div>
                            <div className={"col-sm"}>0-10</div>
                        </div>

                        <div className={"legend-text-about mt-2"}>
                        Цвет обозначает
                        относительный уровень количества НКО на 100 тысяч человек населения. Мы ориентируемся на лидеров
                        – если в городе или районе количество СО НКО равно 80 и более, то цвет будет зелёным, меньше 40
                        – красным, ярко красным – когда совсем мало.
                        </div>

                    </div>

                    <div className={"legend-text"}>
                        <p> Интерактивная карта «Атлас НКО» помогает визуализировать «насыщенность»
                            социально-ориентированными (СО) некоммерческими
                            организациями (НКО) крупных городов и муниципальных районов Южного федерального округа.</p>
                        <p>Источниками данных для карты являются открытые сведения Минюста России, Фонда президентских
                            грантов, региональных правительств и администраций, данные муниципалитетов.</p>
                        <p>Карту можно удалять/приближать, перемещаться по ней. В центре кружочков – количество СО НКО в
                            данном административном образовании (городском округе или муниципальном районе).
                            Дополнительная информация доступна при наведении
                            курсора мыши на кружок с данными.</p>
                        <p>Чуть позже карта дополнится возможностью вывода всех НКО на выбранной территории с названиями
                            и
                            адресами и открытыми датасетами, доступными для верификации.</p>

                    </div>
                </div>
            </div>

        </React.Fragment>
)
}
export default Sidebar;