import React, {useEffect, useRef, useState} from 'react';
import Card from "../Card/Card";
//подключение библиотеки пагинации
import ReactPaginate from 'react-paginate';


const FormSearch = () => {
    const [data, setData] = useState([])
    const [pageData, setPageData] = useState([])
    const [query, setQuery] = useState("")
    const [areas, setAreas] = useState([])
    const [filterArea, setFilterArea] = useState("")
    const [filterStatus, setFilterStatus] = useState("")
    const [isFetching, setFetching] = useState(true)
    const [apiParams, setApiParams] = useState({})
    const [pageCount, setPageCount] = useState(0)
    const selectedPage = useRef(0)
    const perPage = 10
    let offset = 0

// запрос фильтра город / район
//    useEffect(() => {
//        fetch("area/")
//            .then(response => {
//                if (response.status > 400) {
//                    alert("Something went wrong!")
//                }
//                return response.json();
//            })
//            .then(data => {
//                setAreas(data)
//            });
//
//    }, []);

// запрос данных в зависимости от фильтра
    useEffect(() => {
        const url = new URL(`${new URL(window.location.href).origin}/api/v1/nco/`)
        Object.keys(apiParams).forEach(key => url.searchParams.append(key, apiParams[key]))
        fetch(url)
            .then(response => {
                if (response.status > 400) {
                    alert("Something went wrong!")
                }
                return response.json();
            })
            .then(data => {
                setData(data)
                setFetching(false)
            });
    }, [apiParams]);

    //формирование карточек (пагинация) (срабатывает каждый раз, когда применяется фильтр)
    useEffect(() => {
        selectedPage.current = 0
        const slice = data.slice(offset, offset + perPage);
        setPageData(slice.map((card, id) => (
            <Card key={id} {...card}/>
        )))
        setPageCount(Math.ceil(data.length / perPage))
    }, [data, offset])

// Фильтр по городам и районам
    const filteringArea = () => {
        return (
            <div>
                <div className={"filter row"}>
                    <div className={"form-group col-sm"}>
                        <select className={"form-control-black"} name={"area"} id={"id_area"}
                                onChange={event => setFilterArea(event.currentTarget.value)}>
                            <option value={""}>Город / район</option>
                            {areas.map(area => {
                                return (
                                    <option key={area.area} value={area.area}>{area.area}</option>
                                );
                            })}
                        </select>
                    </div>
                    {filteringStatus()}
                </div>
                {searchAndFilterButton()}
            </div>
        )
    }

//Фильтр по статусам
    const filteringStatus = () => {
        return (
            <div className={"form-group col-sm"}>
                <select className={"form-control-black"} name="living_status" id="id_status"
                        onChange={event => setFilterStatus(event.currentTarget.value)}>
                    <option value={""}>Фильтр по статусу</option>
                    <option value={"0"}>Ликвидирована</option>
                    <option value={"1"}>Ликвидируется</option>
                    <option value={"2"}>Реорганизация</option>
                    <option value={"3"}>Недавно созданные</option>
                    <option value={"4"}>Не сдают отчёты</option>
                    <option value={"6"}>Не сдала отчётность о расходах, но сдаёт отчёты о деятельности в минюст</option>
                    <option value={"7"}>Существует, сдаёт всю отчётность в минюст</option>
                </select>
            </div>
        )
    }

//Кнопка поиска и фильтрации
    const searchAndFilterButton = () => {
        const styleButton = {
            letterSpacing: '0.5px'
        }
        return (
            <div className={"row"}>
                <div className={"col-sm-3"}>
                    <button type={"submit"} className={"btn btn-primary btn-block"} style={styleButton}>Фильтр</button>
                </div>
            </div>
        )
    }

    const inputStyle = {
        color: 'black',
        border: '1px solid black'
    }
//обработка кнопок пагинации (стрелочки)
    const handlePageClick = (e) => {
        selectedPage.current = e.selected;
        offset = selectedPage.current * perPage;
        const slice = data.slice(offset, offset + perPage);
        setPageData(slice.map((card, id) => (
            <Card key={id} {...card}/>
        )))
    }
//загрузка
    const loadingState = () => {
        return (<div className="container loading">
            <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>)
    }

    //вывод данных {pageData} и пагинации
    const content = () => {
        return (
            <div className={"container"}>
                <div className={"pg"}>
                    <ReactPaginate
                        forcePage={selectedPage.current}
                        previousLabel={<i className="fa fa-arrow-left"/>}
                        nextLabel={<i className="fa fa-arrow-right"/>}
                        breakLabel={"..."}
                        breakClassName={"break-me"}
                        pageCount={pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={1}
                        onPageChange={handlePageClick}
                        containerClassName={"pagination"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}/>
                </div>
                <div>
                    {pageData}
                </div>
                <div className={"pg"}>
                    <ReactPaginate
                        forcePage={selectedPage.current}
                        previousLabel={<i className="fa fa-arrow-left"/>}
                        nextLabel={<i className="fa fa-arrow-right"/>}
                        breakLabel={"..."}
                        breakClassName={"break-me"}
                        pageCount={pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={1}
                        onPageChange={handlePageClick}
                        containerClassName={"pagination"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}/>
                    <br/>
                </div>
            </div>
        )
    }

    //формирование параметров запроса
    const getFromApi = (e) => {
        e.preventDefault()
        const temp = {}
        if (query !== "") temp.search = query
        if (filterArea !== "") temp.area = filterArea
        if (filterStatus !== "") temp.living_status = filterStatus
        setApiParams(temp)
    }
//вывод (и поиск)
    return (
        <form onSubmit={getFromApi} method="get">
            <div className={"search-block"}>
                <div className={"container"}>
                    <div className={"input-group mb-3"}>
                        <input type={"text"} className={"form-control"} name={"q"}
                               placeholder={"Введите данные"} aria-label={"Recipient's username"}
                               aria-describedby={"basic-addon2"} style={inputStyle}
                               onChange={e => setQuery(e.target.value)}>
                        </input>
                    </div>
                    <div className={"filterT"}><h4><b>Фильтры поиска</b></h4></div>
                    {filteringArea()}
                </div>
            </div>
            {isFetching ? loadingState() : content()}
        </form>
    );
}
export default FormSearch;