import React, { Component } from "react";
import NotFound from '../../General/NotFound/NotFound'
import Footer from "../../General/Footer/Footer";

class DetailCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loaded: false,
            placeholder: ""
        };
    }

    componentDidMount() {
        const {
            match: { params }
        } = this.props;

        const { id } = params;

        fetch("/api/v1/nco/?search=" + id)
            .then(response => {
                if (response.status > 400) {
                    return this.setState(() => {
                        return { placeholder: "Something went wrong!", };
                    });
                }
                return response.json();
            })
            .then(data => {
                this.setState(() => {
                    return {
                        data,
                        loaded: true,
                    };
                });
            });
    }

    fullOkved(obj) {
        console.log(obj);
        return (
            <>
              {obj && obj.map((obj, index) => (
                <p key={index}><b>{obj.code}</b>: {obj.name}</p>
              ))}
            </>
          );
        }
        
    fullManagers(obj) {
        console.log(obj);
        return (
            <>
              {obj && obj.map((obj, index) => (
                <p key={index}>{obj.fio.source}</p>
              ))}
            </>
          );
        }

    fullDataCard() {
        if(this.state.data.length == 1){
            return (
                <div>
                    {this.state.data.map(card => {
                        return (
                            <div key={card.data.ogrn}>
                                <div className={"title"}>
                                    <div className="container">
                                        <span className="cardT2">{card.data.name.full}</span>
                                    </div>
                                </div>
                                <hr/>
                                <div id="content">
                                <div className="container">
                                    <div className="row">

                                        {/*первый столбец*/}
                                            <div className="col-sm">
                                                <div id="left">
                                                    <div className={"left-sticky"}>
                                                    <br/>
                                                    <div className="info1 separator"><h4>Общая информация</h4>
                                                    </div>

                                                    <p><b>ОГРН: </b>{card.data.ogrn}<br/></p>
                                                    <p><b>ИНН: </b>{card.data.inn}<br/></p>
                                                        { typeof(card.data.management) === "undefined" ? (
                                                            <p>
                                                                <b>
                                                                Нет сведений о руководителе организации
                                                                </b>
                                                             </p>)
                                                            : (<p>
                                                                   <b>
                                                                       {card.data.management.post.toUpperCase() + ' '}
                                                                   </b>
                                                                   {card.data.management.name}
                                                               </p>
                                                          ) }
                                                        <p><b>СТАТУС: </b>{card.living_status === '0' ? 'Ликвидирована'
                                                                        : card.living_status === '1' ? 'Ликвидируется'
                                                                        : card.living_status === '2' ? 'Реорганизация'
                                                                        : card.living_status === '3' ? 'Недавно созданна'
                                                                        : card.living_status === '4' ? 'Не сдаёт отчётность в минюст'
                                                                        : card.living_status === '6' ? 'Не сдала отчётность о расходах, но сдаёт отчёты о деятельности в минюст'
                                                                        : card.living_status === '7' ? 'Существует, сдаёт всю отчётность в минюст'
                                                                        : 'Нет информации'}
                                                                        <br/></p>
                                                        <p><b>АДРЕС: </b>{card.data.address.unrestricted_value}</p>
                                                    {/*<div className="info1 separator"><h4>Контакты</h4></div>
                                                    <br/>*/}
                                                 </div>
                                             </div>
                                        </div>
                                        {/*второй столбец*/}
                                                <div className="col-sm">
                                                    <div id="right">
                                                        <br/>
                                                        <div className="info1 separator">
                                                            <h4>Дополнительная информация</h4>
                                                        </div>
                                                            {(typeof card.data.okved) !== 'undefined' ? <p><b>Основной код ОКВЭД: </b> {card.data.okved}<br/></p>: '' }
                                                            {(typeof card.data.opf) !== 'undefined' ? <p><b>Тип организации: </b>{card.data.opf.full}<br/></p> : ''}
                                                            <p><b>Список всех кодов ОКВЭД:</b></p>
                                                            {(typeof this.fullOkved(card.data.okveds)) !== 'undefined' ? this.fullOkved(card.data.okveds) : ''}
                                                            <p><b>Руководители компании:</b>{typeof this.fullManagers(card.data.managers) !== 'undefined' ? this.fullManagers(card.data.managers) : ''}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                    </div>
                                </div>

                            </div>
                        )
                    }
                    )
                    }
                </div>

            )
        } 
        else if (!this.state.loaded) {
            return <div>{this.state.placeholder}</div>
            } else {
                return <NotFound/>
            }
        }

    render() {
        return (
            <React.Fragment>
                <div className={"grid-main"}>
                {this.fullDataCard()}
                </div>
                <Footer/>
            </React.Fragment>
        );
    }
}
export default DetailCard;