import React from 'react';
import { Link, BrowserRouter as Router, Route, useParams } from 'react-router-dom';

const Card = (props) => {
    const card = props
    // console.log(card)
    // console.log('Отработал')
    const colorText = {
        color: 'blue',
    }
    const leader = typeof(card.data.management) === "undefined" ? (
    <p>
        <b>
            Нет сведений о руководителе организации
        </b>
    </p>)
    : (<p>
           <b>
               {card.data.management.post.toUpperCase() + ' '}
           </b>
           {card.data.management.name}
       </p>
      );
    return (
        <>
        <div className={"card"} key={card.data.ogrn}>
            <h5 key={card.data.ogrn}>
                <Link to={`/card/${card.data.ogrn}`} style={colorText}>{card.data.name.full}</Link>
            </h5>
            <h6 className={"card-subtitle mb-2 text-muted"}>
                ОГРН: {card.data.ogrn} | {card.living_status === '0' ? 'Ликвидирована'
                                    : card.living_status === '1' ? 'Ликвидируется'
                                    : card.living_status === '2' ? 'Реорганизация'
                                    : card.living_status === '3' ? 'Недавно созданна'
                                    : card.living_status === '4' ? 'Не сдаёт отчётность в минюст'
                                    : card.living_status === '5' ? 'Не сдаёт отчётность о деятельности, но сдала отчёт о расходах'
                                    : card.living_status === '6' ? 'Не сдала отчётность о расходах, но сдаёт отчёты о деятельности в минюст'
                                    : card.living_status === '7' ? 'Существует, сдаёт всю отчётность в минюст'
                                    : 'Нет информации'}
            </h6>
            {leader}
            <p>{card.data.address.unrestricted_value}</p>
        </div>
        </>
    );
}

export default Card;