import React, { Component } from 'react';

import Title from './MainPage/Title/Title';
import Footer from './General/Footer/Footer';
import FormSearch from './MainPage/FormSearch/FormSearch';


class App extends Component {
  render() {
    return (
      <React.Fragment>
          <div className={"grid-main"}>
        <Title/>
        <FormSearch/>
          </div>
          <Footer/>
      </React.Fragment>

    )
  }
}

export default App;