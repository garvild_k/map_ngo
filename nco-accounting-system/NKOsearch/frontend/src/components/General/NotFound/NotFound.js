import React from 'react';

export default () => {
    return (
        <React.Fragment>
			<div className={"grid-main"}>
			<link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet"></link>
            	<div id="notfound">
					<div className="notfound">
						<div className="notfound-404">
							<div></div>
							<h1>404</h1>
						</div>
						<h2>Страница не найдена</h2>
						<p>Страница, которую вы ищете, могла быть удалена из-за изменения ее названия или временно недоступна.</p>
						<a href="/">Вернуться на главную</a>
					</div>
				</div>
				</div>
        </React.Fragment>
    );
}
