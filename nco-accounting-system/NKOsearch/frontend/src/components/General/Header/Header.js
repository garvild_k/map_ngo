import React from 'react'

export default () => {
    const colorWhite = {
        color: 'white'
    }
    return (
        <div className={"grid-header"}>
            <nav className={"navbar navbar-expand-lg navbar-dark bg-primary "}>
                <div className={"container"}>
                    <div className={"vertical-line"}>
                        <span className={"navbar-brand"}><img width="77" height="77"
                                                              src={"/static/images/logo.png"}/></span>
                    </div>
                    <ul className={"navbar-nav ml-3 mr-auto"}>
                        <li className={"nav-item active"}>
                            <a href={"/"} className={"pr-2"} style={colorWhite}>Список </a>
                            <a href={"/map/"} style={colorWhite}> Карта</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    );
}
