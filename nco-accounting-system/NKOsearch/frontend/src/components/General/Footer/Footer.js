import React from 'react'

export default props => {
    const colorWhite = {
        color: 'white'
    }
    return (
        <div className={"grid-footer"}>
        <nav className={"navbar navbar-expand-lg navbar-dark bg-primary fs"}>
            <div className={"container"}>
                <div className={"vertical-line"}>
                    <span className={"navbar-brand"}>НКО<br/>ПОИСК</span>
                </div>
                <ul className={"navbar-nav ml-3 mr-auto"}>
                    <li className={"nav-item active"}>
                        <span style={colorWhite}>Сервис для поиска НКО</span>
                    </li>
                </ul>
            </div>
        </nav>
        </div>
    );
}
