import App from "./components/App";
import { render } from "react-dom";
import React, { Component } from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Map from './components/Map/Map';
import NotFound from './components/General/NotFound/NotFound';
import Header from "./components/General/Header/Header";
import DetailCard from "./components/MainPage/Card/DetailCard";

class Index extends Component {
    render() {
      return (
          <React.Fragment>
            <Router>
                <div className={"grid"}>
                    <Header/>
              <Switch>
                <Route exact path="/" component={App}/>
                  <Route exact path="/card/:id" component={DetailCard}/>
                  <Route exact path="/map" component={Map}/>
                  <Route path="*" component={NotFound} />
              </Switch>
                </div>
            </Router>
              </React.Fragment>
      )
    }
  }
  
  const container = document.getElementById("root");
  render(<Index />, container);

