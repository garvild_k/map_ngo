import React, { Component } from "react";
import NotFound from '../../General/NotFound/NotFound'
import Footer from "../../General/Footer/Footer";

class DetailCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loaded: false,
            placeholder: "Loading"
        };
    }

    componentDidMount() {
        const {
            match: { params }
        } = this.props;

        const { id } = params;

        fetch("/api/v1/nco/?search=" + id)
            .then(response => {
                if (response.status > 400) {
                    return this.setState(() => {
                        return { placeholder: "Something went wrong!", };
                    });
                }
                return response.json();
            })
            .then(data => {
                this.setState(() => {
                    return {
                        data,
                        loaded: true,
                    };
                });
            });
    }

    fullDataCard() {
        if(this.state.data.length == 1){
            return (
                <div>
                    {this.state.data.map(card => {
                        return (
                            <div>
                                <div className={"title"}>
                                    <div className="container">
                                        <span className="cardT2">{card.name}</span>
                                    </div>
                                </div>
                                <hr/>
                                <div id="content">
                                <div className="container">
                                    <div className="row">

                                        {/*первый столбец*/}
                                            <div className="col-sm">
                                                <div id="left">
                                                    <div className={"left-sticky"}>
                                                    <br/>
                                                    <div className="info1 separator"><h4>Общая информация</h4>
                                                    </div>

                                                    <p><b>ОГРН: </b>{card.ogrn}<br/>
                                                        <b>{card.position} </b>{card.owner}<br/>
                                                        <b>Статус: </b>{}<br/>
                                                        <b>Адрес: </b>{card.address}</p>
                                                    <div className="info1 separator"><h4>Контакты</h4></div>
                                                    <br/>
                                                    </div>
                                                </div>
                                        </div>




                                        {/*второй столбец*/}
                                                <div className="col-sm">
                                                    <div id="right">
                                                        <br/>
                                                        <div className="info1 separator"><h4>Дополнительная
                                                            информация</h4>
                                                        </div>

                                                        <p>
                                                            <b>ОКВЭД: </b>{}<br/>
                                                            <b>Численность работников: </b>{}<br/>
                                                            <b>Система налогов: </b>{}<br/>
                                                            <b>Капитал: </b>{}<br/>
                                                            <b>Финансы: </b>{}<br/>
                                                            <b>Документы: </b>{}<br/>
                                                            <b>Лицензии: </b>{}<br/>
                                                        </p>

                                                    </div>
                                                    </div>
                                                </div>
                                    </div>
                                </div>

                            </div>
                        )
                    }
                    )
                    }
                </div>

            )
        }
        if(this.state.data.length == 0){
            return <NotFound/>
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className={"grid-main"}>
                {this.fullDataCard()}
                </div>
                <Footer/>
            </React.Fragment>
        );
    }
}
export default DetailCard;