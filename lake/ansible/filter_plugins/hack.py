import builtins


def exec_return(code, globals=None, locals=None):
    code = "\n    ".join(code.splitlines())
    code = f"""
def _dummy_():
    {code}
_dummy_ = _dummy_()
    """
    globals = dict(globals or builtins.globals())
    locals = dict(locals or builtins.locals())
    exec(code, globals, locals)
    return locals["_dummy_"]


class FilterModule(object):
    def filters(self):
        return {
            'exec_return': exec_return
        }
